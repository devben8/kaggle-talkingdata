"""processing.py"""

from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline, clone
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def split_dataset(dataframe, split_rate, output_variable):
    """
    Split dataset in 4 parts : x_train, y_train, x_test, y_test.
    Args:
        split_rate : explicit
        output_variable : variable to predict

    Return:
        x_train, y_train : train dataset to fit model.
        x_test : use for predictions
        y_test : use for error calculation
    """
    train_set, test_set = train_test_split(dataframe, test_size=split_rate)
    x_train = train_set.drop(output_variable, axis=1)
    y_train = train_set[output_variable]
    x_test = test_set.drop(output_variable, axis=1)
    y_test = test_set[output_variable]
    return x_train, y_train, x_test, y_test

def feature_importance_barplot(model, data, save=False, path=None):
    """
    plot feature importance from a tree model (sklearn.tree)
    data is just to get columns names
    """
    result_decision_tree = pd.DataFrame(
        {"features": list(data.columns),
         "importance": list(model.feature_importances_)})
    print(result_decision_tree)
    figure = sns.barplot(x="importance", y="features", data=result_decision_tree)
    plt.tight_layout()
    if save:
        plt.savefig(path + file_name)
        plt.close()
    else:
        plt.show()

class MergeTransformers(BaseEstimator, TransformerMixin):
    """
    Feature union between pandas dataframe
    """

    def __init__(self, list_of_transformers=[]):
        self.list_of_transformers = list_of_transformers

    def transform(self, X, **transformparamn):
        concatted = pd.concat([transformer.transform(X).reset_index(drop=True)
                               for transformer in
                               self.fitted_transformers_], axis=1)
        return concatted

    def fit(self, X, y=None, **fitparams):
        self.fitted_transformers_ = []
        for transformer in self.list_of_transformers:
            fitted_trans = clone(transformer).fit(X, y=None, **fitparams)
            self.fitted_transformers_.append(fitted_trans)
        return self

class SelectBasicFeatures(BaseEstimator, TransformerMixin):
    """ Select features accorting to feature list """

    def __init__(self, features=["Div"]):
        self.features = features

    def fit(self, df, y=None):
        return self

    def transform(self, df, y=None):
        return df[self.features]

class TransformDate(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        X['click_time'] = pd.to_datetime(X['click_time'])
        X['day'] = X['click_time'].dt.day
        X['hour'] = X['click_time'].dt.hour
        X['minute'] = X['click_time'].dt.minute
        return X[['day','hour','minute']]
